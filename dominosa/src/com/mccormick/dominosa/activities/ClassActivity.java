package com.mccormick.dominosa.activities;

import android.app.Activity;
import android.os.Bundle;

import com.mccormick.dominosa.controller.GridCommon;
import com.mccormick.dominosa.controller.GridController;
import com.mccormick.dominosa.game.GameBoard2;

public class ClassActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		int size = this.getIntent().getIntExtra("size", 5); //TODO set default value somewhere
	    
		//Create grid
		GridController gc = new GridController(size,GridCommon.GridType.Test);
		
		//Create board
		GameBoard2 board = new GameBoard2(this, gc);
		
		//Set content view
		this.setContentView(board);
	}
}
