package com.mccormick.dominosa.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.NumberPicker;

import com.mccormick.dominosa.R;

public class MenuActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.menu_activity);
		
		Button newGameButton = (Button) this.findViewById(R.id.buttonNewGame);
		newGameButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//Launch dialog on button press
				Dialog newGameDialog = new Dialog(v.getContext());
				newGameDialog.setTitle("New Game");
				newGameDialog.setContentView(R.layout.new_game_dialog);
				
				final NumberPicker np = (NumberPicker) newGameDialog.findViewById(R.id.numberPicker);
				
				Button confirmButton = (Button) newGameDialog.findViewById(R.id.buttonConfirm);
				confirmButton.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(v.getContext(), ClassActivity.class);
						intent.putExtra("size", np.getValue());
						startActivity(intent);
					}
				});
				
				
				np.setMaxValue(10); //TODO store min and max values somewhere
				np.setMinValue(0);
				np.setWrapSelectorWheel(false);
				
				newGameDialog.setCancelable(true);	
				newGameDialog.show();				
			}
		});
		
				
	}

}
