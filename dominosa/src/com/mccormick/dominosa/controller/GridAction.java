package com.mccormick.dominosa.controller;

import java.util.List;

public class GridAction {
	
	public static enum ActionType {
	    Highlight, Unhighlight, 
	    Join, Unjoin, 
	    DrawWall, EraseWall 
	}
	
	// Variables
	private List<Position> cells;
	private ActionType action;
	
	// Constructor
	public GridAction(List<Position> cells, ActionType action) {
		super();
		this.cells = cells;
		this.action = action;
	}
	
	
	public List<Position> getCells() {
		return cells;
	}

	public ActionType getAction() {
		return action;
	}
	
	

}
