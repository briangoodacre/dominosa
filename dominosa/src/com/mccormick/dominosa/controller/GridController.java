package com.mccormick.dominosa.controller;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.mccormick.dominosa.model.Grid;
import com.mccormick.dominosa.model.GridFactory;
import com.mccormick.dominosa.model.GridHelp;

public class GridController {
	
	// local variable
	private Grid myGrid;
	
	// Constructor	
	public GridController(int seedNum, GridCommon.GridType type) {
		super();
		myGrid = GridFactory.getGrid(seedNum, type);
	}

	// Actions
	
	public List<GridAction> joinGridCells(Position pos1, Position pos2)
	{	
		Log.d(this.getClass().getName(), "joinGridCells");
		
		return new ArrayList<GridAction>();
	}
	
	public List<GridAction> unjoinGridCells(Position cell)
	{
		Log.d(this.getClass().getName(), "unjoinGridCells");

		return new ArrayList<GridAction>();
	}
	
	public boolean isGridComplete()
	{
		return false;
	}	
	
	public List<GridAction> highlightGridCells(int numToHighlight1, int numToHighlight2)
	{
		return new ArrayList<GridAction>();
	}
	
	public List<GridAction> drawWall(Position pos1, Position pos2)
	{
		return new ArrayList<GridAction>();
	}
	
	public List<GridAction> eraseWall(Position pos1, Position pos2)
	{
		return new ArrayList<GridAction>();
	}
	
	// Getters of the Grid
	
	public int getValue(Position pos)
	{
		return myGrid.getGridCell(pos.getRowPosition(), pos.getColPosition()).getValue();
	}
	public int getNumRows()
	{
		return GridHelp.getRowsFromSeed(myGrid.getSeed());
	}
	public int getNumColumns()
	{
		return GridHelp.getColumnsFromSeed(myGrid.getSeed());
	}

}
