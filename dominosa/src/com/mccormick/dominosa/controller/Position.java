package com.mccormick.dominosa.controller;

public class Position {
	
	// local variables
	private int rowPosition;
	private int colPosition;
	
	/**
	 * Constructor for Position
	 * @param rowPosition
	 * @param colPosition
	 */
	public Position(int rowPosition, int colPosition) {
		super();
		this.rowPosition = rowPosition;
		this.colPosition = colPosition;
	}

	// Getters
	public int getRowPosition() {
		return rowPosition;
	}

	public int getColPosition() {
		return colPosition;
	}	

}
