package com.mccormick.dominosa.game;

import android.content.Context;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.mccormick.dominosa.controller.Position;

public class GameCell extends TextView {
	private Position position;
	
	public GameCell(Context context) {
		super(context);
		this.setGravity(Gravity.CENTER);
		this.setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT, 1.0f));
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
}
