package com.mccormick.dominosa.model;


public class Grid 
{
	private GridCell[][] myGrid;
	
	// sizing dimensions
	private int numRows;
	private int numColumns;
	private int seed;
	
	// walls
	
	// highlighted numbers
	private Integer numberHighlight1;
	private Integer numberHighlight2;
	
	/**
	 * Grid Constructor
	 * 
	 * @param seed
	 */
	public Grid(int seed)
	{
		super();
		myGrid = new GridCell[seed+1][seed+2];
		
		// save sizing numbers
		this.seed = seed;
		this.numRows = GridHelp.getRowsFromSeed(seed);
		this.numColumns = GridHelp.getColumnsFromSeed(seed);
		
		// save highlighted numbers
		this.numberHighlight1 = null;
		this.numberHighlight2 = null;
	}	

	public GridCell getGridCell(int row, int column)
	{
		return myGrid[row][column];
	}
	
	public GridCell getGridCell(int index)
	{
		int row = index/numColumns;
		int col = index%numColumns;
		return myGrid[row][col];
	}

	public GridCell[][] getMyGrid() {
		return myGrid;
	}

	public void setMyGrid(GridCell[][] myGrid) {
		this.myGrid = myGrid;
	}

	
	// Getters
	public int getNumRows() {
		return numRows;
	}

	public int getNumColumns() {
		return numColumns;
	}

	public int getSeed() {
		return seed;
	}
	
	

}
