package com.mccormick.dominosa.model;

import android.util.Log;

public class GridCell{
	
	// TODO - change
	private static final String logTag = Class.class.getName();
	
	// inherited
	// color
	// value
	
	// variables
	private GridCell partnerCell;
	private int value;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	// Constructor
	public GridCell(int rowPosition, int columnPosition) {
		Log.v(logTag,"Made a GridCell");
	}

	// Getters and Setters
	public GridCell getPartnerCell() {		
		return partnerCell;
	}

	public void setPartnerCell(GridCell partnerCell) {
		this.partnerCell = partnerCell;
	}

	public boolean isSelected()
	{
		return null == partnerCell;
	}
	

}
