package com.mccormick.dominosa.model;

import com.mccormick.dominosa.controller.GridCommon;

public class GridFactory {
	
	public static Grid getGrid(int seedNum, GridCommon.GridType type)
	{
		switch (type) {
	        case Test:  
	        	return buildTestGrid(seedNum);
	        case Load:
	        	return findGrid(seedNum);
	        case Build:
	        	return buildGrid(seedNum);
        	default: 
        		return new Grid(seedNum);
		}
	}
	
	private static Grid buildGrid(int seedNum)
	{
		// TODO
		
		// for testing
		return buildTestGrid(GridHelp.getRowsFromSeed(seedNum));
	}
	
	private static Grid findGrid (int seed)
	{
		GridCell newGridCell;
		int[][] existingGrid = loadGrid(seed);
		Grid gridToBuild = new Grid(seed);
		
		// Iterate and build the array list in the Grid
		for (int i = 0; i < GridHelp.getRowsFromSeed(seed); i++) {
			for (int j = 0; j < GridHelp.getColumnsFromSeed(seed); j++) {
				// The GridCell and value
				newGridCell = new GridCell(i, j);
				newGridCell.setValue(existingGrid[i][j]);
				
				// Add to Grid
				gridToBuild.getMyGrid()[i][j]=newGridCell;
			}
		}
		
		return gridToBuild;
	}
	
	private static int[][] loadGrid(int seedNum)
	{
		// TODO - get from file
		
		if (seedNum == 3)
		{
			return grid3x3rows;
		}
		else if (seedNum == 4)
		{
			return grid4x4rows;
		}
		else
		{
			return new int[0][0];
		}
	}
	
	private static Grid buildTestGrid(int seed){
		int numRows = GridHelp.getRowsFromSeed(seed);
		int numCol = GridHelp.getColumnsFromSeed(seed);
		GridCell[][] gridCellList = new GridCell[numRows][numCol];
		for (int r=0; r<numRows; r++){
			for (int c=0; c<numCol; c++){
				GridCell gc = new GridCell(r,c);
				gc.setValue(r*(numRows+1) + c);
				gridCellList[r][c]=gc;
			}
		}
		Grid returnGrid = new Grid(seed);
		returnGrid.setMyGrid(gridCellList);
		return returnGrid;
	}
	
		
	
	private static int[][] grid3x3rows = {
			{0,0,2,3,0},
			{2,1,2,2,0},
			{3,1,1,0,2},
			{3,1,1,3,3}
			};
	private static int[][] grid4x4rows = {
			{3,3,3,3,2,2},
			{4,0,0,2,2,2},
			{1,0,4,1,1,3},
			{1,0,1,3,4,4},
			{0,0,4,4,2,1}
			};
	
	
}
