package com.mccormick.dominosa.model;

public class GridHelp {
	
	public static int getRowsFromSeed(int seed)
	{
		return seed+1;
	}
	public static int getColumnsFromSeed(int seed)
	{
		return seed+2;
	}
	public static int getSeedFromRows(int rows)
	{
		return rows-1;
	}
	public static int getSeedFromColumns(int columns)
	{
		return columns-2;
	}

}
