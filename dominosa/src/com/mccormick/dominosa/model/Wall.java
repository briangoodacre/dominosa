package com.mccormick.dominosa.model;

import com.mccormick.dominosa.controller.Position;

public class Wall {
	
	// local variables
	private Position pos1;
	private Position pos2;
	
	/**
	 * Constructor for the Wall
	 * @param pos1
	 * @param pos2
	 */	
	public Wall(Position pos1, Position pos2) {
		super();
		this.pos1 = pos1;
		this.pos2 = pos2;
	}
	
	// Getters
	public Position getPos1() {
		return pos1;
	}
	public Position getPos2() {
		return pos2;
	}
	
	
	
	

}
